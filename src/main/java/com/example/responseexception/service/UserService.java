package com.example.responseexception.service;

import com.example.responseexception.dto.UserCreateDto;
import com.example.responseexception.dto.UserDto;
import com.example.responseexception.exception.UserNotFoundException;
import com.example.responseexception.model.User;
import com.example.responseexception.repositroy.UserRepository;
import jakarta.persistence.EntityExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;


    public UserDto addUser(final UserCreateDto createDto){
        boolean isExist = userRepository.existsByEmail(createDto.getEmail());
        if (!isExist){
            User user = User.builder()
                    .email(createDto.getEmail())
                    .firstName(createDto.getFirstName())
                    .lastNme(createDto.getLastName())
                    .password(createDto.getPassword())
                    .build();
             userRepository.save(user);
            return new UserDto(user.getEmail(), user.getFirstName(), user.getLastNme());
        }
        throw new EntityExistsException("This email already exist");
    }
    public User getById(Long id){
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()){
            return userOptional.get();
        }
        throw new UserNotFoundException("User not found");
    }
}
