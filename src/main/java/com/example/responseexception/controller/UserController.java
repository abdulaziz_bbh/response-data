package com.example.responseexception.controller;

import com.example.responseexception.dto.UserCreateDto;
import com.example.responseexception.dto.UserDto;
import com.example.responseexception.model.User;
import com.example.responseexception.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/create-user")
    public UserDto addUser(@RequestBody final UserCreateDto createDto){
        return userService.addUser(createDto);
    }@GetMapping("/get-by-id/{id}")
    public User getById(@PathVariable("id") final Long id){
        return userService.getById(id);
    }
}
