package com.example.responseexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResponseExceptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResponseExceptionApplication.class, args);
	}

}
