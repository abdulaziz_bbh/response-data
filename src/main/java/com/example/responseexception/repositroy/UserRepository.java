package com.example.responseexception.repositroy;

import com.example.responseexception.model.User;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

     boolean existsByEmail(String email);

     //Optional<User> fin(Long id);


}
